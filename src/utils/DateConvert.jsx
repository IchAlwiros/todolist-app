export function dateConverterID(date) {
  const timestamp = new Date(date);
  const options = { year: "numeric", month: "numeric", day: "numeric" };
  const result = timestamp.toLocaleDateString("id-ID", options);

  return result;
}

export function dateConverterEnUS(date) {
  const timestamp = new Date(date);
  const options = { year: "numeric", month: "numeric", day: "numeric" };
  const result = timestamp.toLocaleDateString("en-US", options);

  return result;
}

export function dateMonthLongConverter(date) {
  const timestamp = new Date(date);
  const options = { year: "numeric", month: "long", day: "numeric" };
  const result = timestamp.toLocaleDateString("id-ID", options);

  return result;
}
