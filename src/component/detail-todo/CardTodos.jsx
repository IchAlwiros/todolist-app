import { Badge, Button, Card, Checkbox } from "antd";
import { DeleteFilled, EditOutlined } from "@ant-design/icons";

import axios from "axios";
import { TodoContext } from "../../context/TodoContext";
import { useContext } from "react";
import { createShortText, hostUrl } from "../../utils";

export function CardTodo({ id, title, is_active, priority }) {
  const {
    setCurrentData,
    setOpenEditModal,
    setDeleteConfirm,
    editForm,
    setTodos,
  } = useContext(TodoContext);

  const onChange = (e) => {
    let checked = e.target.checked;
    let idItem = e.target.id;
    axios
      .patch(`${hostUrl}/todo-items/${idItem}`, { is_active: !checked })
      .then((res) => {
        setTodos(null);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleEdit = (title, priority, id) => {
    editForm.setFieldsValue({ title: title, priority: priority, id: id });
  };
  return (
    <>
      <Card
        style={{
          width: "100%",
          margin: "10px 0",
          boxShadow: "0px 6px 10px 0px #0000001A",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            margin: "0 10px",
          }}
        >
          <div
            style={{
              width: "40%",
            }}
          >
            <div
              style={{
                alignItems: "center",
                display: "flex",
                width: "100%",
                textAlign: "start",
              }}
            >
              <Checkbox
                data-cy="todo-item-checkbox"
                style={{ margin: "0 2%" }}
                id={id}
                checked={!is_active ? true : false}
                onChange={onChange}
              />

              <Badge
                data-cy="todo-item-priority-indicator"
                color={
                  priority == "normal"
                    ? "green"
                    : priority == "high"
                    ? "orange"
                    : priority == "very-high"
                    ? "red"
                    : priority == "low"
                    ? "blue"
                    : "purple"
                }
                style={{ margin: "0 20px" }}
              />
              {is_active ? (
                <span data-cy="todo-item-title">
                  {createShortText(title, 30)}
                </span>
              ) : (
                <span
                  data-cy="todo-item-title"
                  style={{ textDecoration: "line-through" }}
                >
                  {createShortText(title, 30)}
                </span>
              )}
              <Button
                data-cy="todo-item-edit-button"
                htmlType="button"
                onClick={() => {
                  handleEdit(title, priority, id);
                  setOpenEditModal(true);
                }}
                type="ghost"
                icon={<EditOutlined />}
              />
            </div>
          </div>
          <Button
            data-cy="todo-item-delete-button"
            type="ghost"
            onClick={() => {
              setDeleteConfirm(true);
              setCurrentData({
                id: id,
                title: title,
              });
            }}
            icon={<DeleteFilled />}
          />
        </div>
      </Card>
    </>
  );
}
