import { ConfigProvider } from "antd";
import MainRoute from "./route/route";
import { ActivityProvider } from "./context/ActivityContext";
import { TodoProvider } from "./context/TodoContext";
import { theme } from "./theme/theme";
import "./App.css";

function App() {
  return (
    <div className="App">
      <TodoProvider>
        <ActivityProvider>
          <ConfigProvider theme={theme}>
            <MainRoute />
          </ConfigProvider>
        </ActivityProvider>
      </TodoProvider>
    </div>
  );
}

export default App;
