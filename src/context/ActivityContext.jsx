import { useState, createContext } from "react";

export const ActivityContext = createContext();

export const ActivityProvider = (props) => {
  const [currentData, setCurrentData] = useState(null);
  const [openConfirm, setOpenConfirm] = useState(false);

  return (
    <ActivityContext.Provider
      value={{
        currentData,
        setCurrentData,
        openConfirm,
        setOpenConfirm,
      }}
    >
      {props.children}
    </ActivityContext.Provider>
  );
};
