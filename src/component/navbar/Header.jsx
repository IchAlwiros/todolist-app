import { Layout } from "antd";

const { Header } = Layout;

export const CustomHeader = () => {
  return (
    <>
      <Header
        data-cy="navbar"
        style={{
          backgroundColor: "#16ABF8",
          boxShadow: "0px 10px 15px -3px rgba(0,0,0,0.1)",
        }}
      >
        <div
          data-cy="header-background"
          style={{
            background: "#16ABF8",
            display: "flex",
          }}
        >
          <h1
            data-cy="header-title"
            style={{
              margin: "0 10% 0 10%",
              paddingLeft: 9,
              color: "white",
            }}
          >
            TO DO LIST APP
          </h1>
        </div>
      </Header>
    </>
  );
};
