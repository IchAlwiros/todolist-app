import { Col, Layout, Row } from "antd";
import { Outlet } from "react-router-dom";
import { CustomHeader } from "../component/";

const { Content } = Layout;

export const MainLayout = () => {
  return (
    <>
      <div
        className="wrapper"
        style={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
          justifyContent: "space-between",
        }}
      >
        <div className="navbar">
          <CustomHeader />
          <Layout>
            <Row justify="center">
              <Col span={18}>
                <Content style={{ padding: 0 }}>
                  <div
                    className="site-layout-content"
                    style={{ marginTop: "10px" }}
                  >
                    <Outlet />
                  </div>
                </Content>
              </Col>
            </Row>
          </Layout>
        </div>
      </div>
    </>
  );
};
