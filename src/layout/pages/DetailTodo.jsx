import {
  Badge,
  List,
  Select,
  Result,
  Button,
  Dropdown,
  Row,
  Form,
  Input,
  Col,
  Modal,
  Spin,
  message,
} from "antd";
import {
  PlusOutlined,
  LeftOutlined,
  EditOutlined,
  SortAscendingOutlined,
  LoadingOutlined,
  InfoCircleOutlined,
} from "@ant-design/icons";

import axios from "axios";
import { useContext, useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { Image2 } from "../../assets";
import { hostUrl } from "../../utils/QuickEnv";
import { CardTodo } from "../../component";
import { TodoContext } from "../../context/TodoContext";

const { Option } = Select;

const items = [
  {
    key: "1",
    label: (
      <Button data-cy={`sort-selection`} type="ghost">
        <a data-cy="todo-item-title`">Terbaru</a>
      </Button>
    ),
  },
  {
    key: "2",
    label: (
      <Button data-cy={`sort-selection`} type="ghost">
        <a data-cy="todo-item-title`">Terlama</a>
      </Button>
    ),
  },
  {
    key: "3",
    label: (
      <Button data-cy={`sort-selection`} type="ghost">
        <a data-cy="todo-item-title`">A-Z</a>
      </Button>
    ),
  },
  {
    key: "4",
    label: (
      <Button data-cy={`sort-selection`} type="ghost">
        <a data-cy="todo-item-title`">Z-A</a>
      </Button>
    ),
  },
  {
    key: "5",
    label: (
      <Button data-cy={`sort-selection`} type="ghost">
        <a data-cy="todo-item-title`">Belum Selesai</a>
      </Button>
    ),
  },
];

export const DetailTodo = () => {
  const {
    currentData,
    openEditModal,
    setOpenEditModal,
    setDeleteConfirm,
    deleteConfirm,
    editForm,
    todos,
    setTodos,
  } = useContext(TodoContext);

  const [dataSort, setSort] = useState(null);
  const [open, setOpen] = useState(false);
  const [informationOpen, setinformationOpen] = useState(false);
  // let [disabledButton, setDisabledButton] = useState(false);
  const [form] = Form.useForm();

  const inputRef = useRef(null);
  const titleRef = useRef(null);
  let { id } = useParams();
  const navigate = useNavigate();
  useEffect(() => {
    if (id) {
      if (todos === null) {
        axios
          .get(`${hostUrl}/activity-groups/${id}`)
          .then((res) => {
            const { data } = res;
            inputRef.current.setFieldsValue({
              title: data?.title,
            });
            setTodos(data?.todo_items);
          })
          .catch((err) => {
            if (err.message === "Network Error") {
              message.error("Network Error");
            } else message.error("Error Internal Server");
          });
      }

      if (todos === "sort") {
        setTodos(dataSort);
      }
    }
  }, [id, todos]);

  const sharedProps = {
    style: {
      width: "100%",
      fontSize: "20px",
    },
    defaultValue: todos?.title,
    ref: titleRef,
  };

  const onFinish = async (value) => {
    setOpen(false);
    axios
      .post(`${hostUrl}/todo-items`, {
        activity_group_id: id,
        title: value.title,
        priority: value.priority,
      })
      .then((res) => {
        form.resetFields();
        setTodos(null);
      })
      .catch((error) => {
        message.error(error, message);
      });
  };

  const hendleDelete = (id) => {
    setDeleteConfirm(false);
    axios
      .delete(`${hostUrl}/todo-items/${id}`)
      .then((res) => {
        setTodos(null);
        setinformationOpen(true);
      })
      .catch((error) => {
        message.error(error);
      });
  };

  const confirmUpdate = (value) => {
    setOpenEditModal(false);
    axios
      .patch(`${hostUrl}/todo-items/${value.id}`, {
        title: value.title,
        priority: value.priority,
      })
      .then((res) => {
        editForm.resetFields();
        setTodos(null);
      })
      .catch((error) => {
        message.error(error.message);
      });
  };
  // const onChangeForm = (e) => {
  //   console.log(e);
  //   e ? setDisabledButton(false) : setDisabledButton(true);
  // };
  // const onFailedInputForm = (errorInfo) => {
  //   // console.log(e);
  //   errorInfo ? setDisabledButton(true) : setDisabledButton(false);
  // };

  const onClick = ({ key }) => {
    switch (key) {
      case "1":
        const newst = todos?.sort((a, b) => {
          const orderA = a.id;
          const orderB = b.id;

          if (orderA > orderB) {
            return -1;
          }
          if (orderA < orderB) {
            return 1;
          }
          return 0;
        });
        setSort(newst);
        setTodos("sort");
        break;
      case "2":
        const oldest = todos?.sort((a, b) => {
          const orderA = a.id;
          const orderB = b.id;

          if (orderA < orderB) {
            return -1;
          }
          if (orderA > orderB) {
            return 1;
          }
          return 0;
        });
        setSort(oldest);
        setTodos("sort");
        break;
      case "3":
        const asc = todos?.sort((a, b) => {
          const titleA = a.title.toLowerCase();
          const titleB = b.title.toLowerCase();

          if (titleA < titleB) {
            return -1;
          }
          if (titleA > titleB) {
            return 1;
          }
          return 0;
        });
        setSort(asc);
        setTodos("sort");
        break;
      case "4":
        const desc = todos?.sort((a, b) => {
          const titleA = a.title.toLowerCase();
          const titleB = b.title.toLowerCase();

          if (titleA > titleB) {
            return -1;
          }
          if (titleA < titleB) {
            return 1;
          }
          return 0;
        });
        setSort(desc);
        setTodos("sort");
        break;
      case "5":
        const notfinish = todos?.sort((a, b) => {
          const statusA = a.is_active;
          const statusB = b.is_active;

          if (statusA < statusB) {
            return -1;
          }
          if (statusA > statusB) {
            return 1;
          }
          return 0;
        });
        setSort(notfinish);
        setTodos("sort");
        break;
      default:
        break;
    }
  };

  const onTitleChange = (e) => {
    id = Number(id);
    axios
      .patch(`${hostUrl}/activity-groups/${id}`, { title: e?.title })
      .then((res) => {})
      .catch((error) => {
        message.error(error.message);
      });
  };
  function InformationModal() {
    return (
      <>
        <div data-cy="modal-information">
          <Modal
            centered
            open={informationOpen}
            width={500}
            onCancel={() => {
              setinformationOpen(false);
            }}
            footer={[]}
          >
            <div
              data-cy="modal-information-text"
              style={{
                display: "flex",
                alignItems: "center",
                textAlign: "start",
                justifyContent: "space-evenly",
              }}
            >
              <p>
                <InfoCircleOutlined style={{ color: "red", fontSize: 22 }} />{" "}
                Berhasil menghapus Todo
              </p>
            </div>
          </Modal>
        </div>
      </>
    );
  }
  function EditModal() {
    return (
      <>
        <Modal
          title="Edit Item"
          centered
          open={openEditModal}
          width={700}
          onCancel={() => {
            setOpenEditModal(false);
          }}
          footer={[]}
        >
          <Form
            data-cy="todo-input-dialog-body"
            layout="vertical"
            style={{ textAlign: "left" }}
            form={editForm}
            onFinish={confirmUpdate}
            // onFinishFailed={onFailedInput}
          >
            <Form.Item name="id" />
            <Form.Item
              data-cy="todo-input-title-label"
              name="title"
              label="TITLE"
              rules={[{ required: true }]}
            >
              <Input data-cy="modal-add-name-input" />
            </Form.Item>
            <div data-cy="modal-add-priority-dropdown">
              <Form.Item
                data-cy="todo-input-priority-label"
                name="priority"
                label="PRIORITY"
                rules={[{ required: true }]}
              >
                <Select
                  data-cy="modal-add-priority-dropdown"
                  placeholder="Pilih Opsi"
                  allowClear
                >
                  <Option data-cy="modal-add-priority-item" value="very-high">
                    <div>
                      <Badge color="red" style={{ marginRight: "10px" }} />
                      Very High
                    </div>
                  </Option>
                  <Option data-cy="modal-add-priority-item" value="high">
                    <div>
                      <Badge color="orange" style={{ marginRight: "10px" }} />
                      High
                    </div>
                  </Option>
                  <Option data-cy="modal-add-priority-item" value="normal">
                    <div>
                      <Badge color="green" style={{ marginRight: "10px" }} />
                      Medium
                    </div>
                  </Option>
                  <Option data-cy="modal-add-priority-item" value="low">
                    <div>
                      <Badge color="blue" style={{ marginRight: "10px" }} />
                      Low
                    </div>
                  </Option>
                  <Option data-cy="modal-add-priority-item" value="very-low">
                    <div>
                      <Badge color="purple" style={{ marginRight: "10px" }} />
                      Very Low
                    </div>
                  </Option>
                </Select>
              </Form.Item>
            </div>

            <Form.Item>
              <Row>
                <Col
                  span={24}
                  style={{
                    textAlign: "right",
                  }}
                >
                  <Button
                    data-cy="modal-add-save-button"
                    size="large"
                    type="primary"
                    htmlType="submit"
                    // disabled={disabledButton}
                  >
                    Simpan
                  </Button>
                </Col>
              </Row>
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  }

  function AddModal() {
    return (
      <>
        <Modal
          data-cy="modal-add"
          title="Tambahkan List Item"
          centered
          open={open}
          onCancel={() => {
            setOpen(false);
          }}
          width={700}
          footer={[]}
        >
          <Form
            data-cy="todo-input-dialog-body"
            layout="vertical"
            onFinish={onFinish}
            // onChange={onChangeForm}
            // onFinishFailed={onFailedInputForm}
            form={form}
            style={{ textAlign: "left" }}
            autoComplete="off"
          >
            <Form.Item
              data-cy="todo-input-title-label"
              name="title"
              label="NAMA LIST ITEM"
              rules={[
                {
                  required: true,
                },
                {
                  type: "string",
                },
              ]}
            >
              <div data-cy="modal-add-name-input">
                <Input placeholder="Tambahkan Nama Activity" />
              </div>
            </Form.Item>
            <div data-cy="modal-add-priority-dropdown">
              <Form.Item
                data-cy="todo-input-priority-label"
                name="priority"
                label="PRIORTY"
                rules={[
                  {
                    required: true,
                  },
                  {
                    type: "string",
                  },
                ]}
              >
                <Select data-cy="modal-add-priority-dropdown">
                  <Option value="very-high">
                    <div>
                      <Badge color="red" style={{ marginRight: "10px" }} />
                      Very High
                    </div>
                  </Option>
                  <Option value="high">
                    <div>
                      <Badge color="orange" style={{ marginRight: "10px" }} />
                      High
                    </div>
                  </Option>
                  <Option value="normal">
                    <div>
                      <Badge color="green" style={{ marginRight: "10px" }} />
                      Medium
                    </div>
                  </Option>
                  <Option value="low">
                    <div>
                      <Badge color="blue" style={{ marginRight: "10px" }} />
                      Low
                    </div>
                  </Option>
                  <Option value="very-low">
                    <div>
                      <Badge color="purple" style={{ marginRight: "10px" }} />
                      Very Low
                    </div>
                  </Option>
                </Select>
              </Form.Item>
            </div>

            <Form.Item>
              <Row>
                <Col
                  span={24}
                  style={{
                    textAlign: "right",
                  }}
                >
                  <Button
                    data-cy="modal-add-save-button"
                    size="large"
                    type="primary"
                    htmlType="submit"
                    // disabled={disabledButton}
                  >
                    Simpan
                  </Button>
                </Col>
              </Row>
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  }

  function DeleteModal() {
    return (
      <>
        <Modal
          centered
          open={deleteConfirm}
          onCancel={() => {
            setDeleteConfirm(false);
          }}
          width={1000}
          footer={[]}
        >
          <Result
            data-cy="confirm-dialog-text"
            status="warning"
            title={`Apakah anda yakin menghapus item ${currentData?.title}`}
            extra={
              <>
                <Button
                  data-cy="modal-delete-cancel-button"
                  onClick={() => {
                    setDeleteConfirm(false);
                  }}
                  type="primary"
                  key="console"
                  style={{
                    background: "#F4F4F4",
                    borderRadius: 20,
                    width: 150,
                    color: "black",
                  }}
                  size="large"
                >
                  Batal
                </Button>
                <Button
                  data-cy="modal-delete-confirm-button"
                  onClick={() => {
                    hendleDelete(currentData?.id);
                  }}
                  style={{
                    background: "#ED4C5C",
                    borderRadius: 20,
                    width: 150,
                  }}
                  type="primary"
                  key="console"
                  size="large"
                >
                  Hapus
                </Button>
              </>
            }
          />
        </Modal>
      </>
    );
  }

  return (
    <div style={{ minHeight: "100vh", padding: 20 }}>
      <DeleteModal data-cy="modal-delete" />
      <InformationModal />
      <AddModal />
      <EditModal data-cy="modal-edit" />
      <div className="content" style={{ marginTop: "10%" }}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: 20,
            padding: "0 20px",
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              width: "30%",
            }}
          >
            <Button
              data-cy="todo-back-button"
              onClick={() => {
                navigate("/");
              }}
              type="text"
              size="large"
              icon={<LeftOutlined />}
            />

            <Form
              ref={inputRef}
              onValuesChange={onTitleChange}
              style={{
                alignItems: "center",
                display: "flex",
                margin: 0,
                height: 0,
              }}
            >
              <div data-cy="todo-title">
                <Form.Item
                  name="title"
                  style={{ margin: 0 }}
                  rules={[
                    {
                      type: "string",
                    },
                  ]}
                >
                  <Input
                    data-cy="todo-title"
                    bordered={false}
                    {...sharedProps}
                  />
                </Form.Item>
              </div>
            </Form>
            <Button
              data-cy="todo-title-edit-button"
              type="ghost"
              size="large"
              icon={<EditOutlined />}
              onClick={() => {
                titleRef.current.focus({
                  cursor: "end",
                });
              }}
            />
          </div>
          <div
            style={{
              width: "20%",
              justifyContent: "space-between",
              display: "flex",
            }}
          >
            <Dropdown
              data-cy="sort-parent"
              menu={{
                items,
                selectable: true,
                onClick,
              }}
              placement="bottomLeft"
              arrow
            >
              <Button
                data-cy="todo-sort-button"
                icon={<SortAscendingOutlined />}
                style={{
                  background: "white",
                  marginRight: "19px",
                }}
                size="large"
                type="ghost"
              />
            </Dropdown>
            <Button
              data-cy="todo-add-button"
              style={{ borderRadius: 20 }}
              type="primary"
              icon={<PlusOutlined />}
              size="large"
              onClick={() => {
                setOpen(true);
              }}
            >
              Tambah
            </Button>
          </div>
        </div>
      </div>
      {todos && todos?.length > 0 ? (
        <>
          <List
            data-cy={`todo-item`}
            dataSource={todos}
            renderItem={(item, index) => {
              return (
                <>
                  <CardTodo
                    id={item?.id}
                    title={item?.title}
                    is_active={item?.is_active}
                    priority={item?.priority}
                  />
                </>
              );
            }}
          />
        </>
      ) : (
        <>
          {todos?.length === 0 ? (
            <>
              <div
                data-cy="todo-empty-state"
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                  cursor: "pointer",
                }}
                onClick={() => {
                  setOpen(true);
                }}
              >
                <img data-cy="todo-empty-state" height={400} src={Image2} />
              </div>
            </>
          ) : (
            <>
              <Spin
                indicator={
                  <LoadingOutlined
                    style={{
                      fontSize: 24,
                    }}
                    spin
                  />
                }
              />
            </>
          )}
        </>
      )}
    </div>
  );
};
