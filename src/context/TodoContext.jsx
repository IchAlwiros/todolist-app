import { Form } from "antd";
import { useState, createContext } from "react";

export const TodoContext = createContext();

export const TodoProvider = (props) => {
  const [currentData, setCurrentData] = useState(null);
  const [openEditModal, setOpenEditModal] = useState(false);
  let [deleteConfirm, setDeleteConfirm] = useState(false);
  const [todos, setTodos] = useState(null);
  const [editForm] = Form.useForm();

  return (
    <TodoContext.Provider
      value={{
        currentData,
        setCurrentData,
        openEditModal,
        setOpenEditModal,
        setDeleteConfirm,
        deleteConfirm,
        editForm,
        todos,
        setTodos,
      }}
    >
      {props.children}
    </TodoContext.Provider>
  );
};
