import { useNavigate } from "react-router-dom";
import { dateMonthLongConverter } from "../../utils";
import { useContext } from "react";
import { ActivityContext } from "../../context/ActivityContext";
import { Button } from "antd";
import { DeleteFilled } from "@ant-design/icons";
import { TodoContext } from "../../context/TodoContext";

export function CardItems({ id, title, created_at }) {
  const navigate = useNavigate();
  const { setCurrentData, setOpenConfirm, openConfirm } =
    useContext(ActivityContext);
  const { setTodos } = useContext(TodoContext);
  return (
    <div
      data-cy={`activity-item`}
      style={{
        height: "234px",
        background: "#fff",
        borderRadius: "12px",
        padding: "22px 27px",
        marginBottom: "26px",
        position: "relative",
        boxShadow: "0 4px 8px rgba(0,0,0,.15)",
        textAlign: "start",
      }}
    >
      <div
        style={{
          cursor: "pointer",
          height: "158px",
        }}
        onClick={() => {
          navigate(`/detail/${id}`);
          setTodos(null);
        }}
      >
        <h4 data-cy="activity-item-title">{title}</h4>
      </div>
      <div
        style={{
          position: "absolute",
          zIndex: 2,
          bottom: "25px",
          display: "flex",
          justifyContent: "space-between",
          width: "calc(100% - 54px)",
          alignItems: "center",
        }}
      >
        <span data-cy="activity-item-date">
          {dateMonthLongConverter(created_at)}
        </span>

        <Button
          data-cy="activity-item-delete-button"
          type="ghost"
          icon={<DeleteFilled />}
          disabled={openConfirm}
          onClick={() => {
            setOpenConfirm(true);
            setCurrentData({
              id: id,
              title: title,
            });
          }}
        />
      </div>
    </div>
  );
}
