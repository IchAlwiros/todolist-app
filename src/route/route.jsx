import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import { MainLayout, DashboardActivity, DetailTodo } from "../layout/index";

const MainRouter = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainLayout />}>
            <Route index element={<DashboardActivity />} />
            <Route path="detail/:id" element={<DetailTodo />} />
          </Route>

          <Route path="*" element={<NoMatchPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

function NoMatchPage() {
  return (
    <div>
      <h2>Nothing to see here!</h2>
      <p>
        <Link to="/">Back to home</Link>
      </p>
    </div>
  );
}

export default MainRouter;
