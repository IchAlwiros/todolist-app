import { List, Button, Result, Modal, Spin, message } from "antd";
import {
  PlusOutlined,
  LoadingOutlined,
  WarningOutlined,
  InfoCircleOutlined,
} from "@ant-design/icons";
import { Image1 } from "../../assets";
import { hostUrl } from "../../utils";
import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { ActivityContext } from "../../context/ActivityContext";
import { CardItems } from "../../component";

export const DashboardActivity = () => {
  const [todo, setTodo] = useState(null);
  const [errorOpen, setErrorOpen] = useState(null);
  const { currentData, openConfirm, setOpenConfirm } =
    useContext(ActivityContext);

  useEffect(() => {
    if (todo === null) {
      axios
        .get(`${hostUrl}/activity-groups?email=ichalwiradev@gmail.com`)
        .then((res) => {
          const { data } = res;
          setTodo(data);
        })
        .catch((err) => {
          if (err.message === "Request failed with status code 500") {
            setErrorOpen(true);
          } else message.error(err);
        });
    }
  }, [todo]);

  const addTodo = async () => {
    axios
      .post(`${hostUrl}/activity-groups`, {
        email: "ichalwiradev@gmail.com",
        title: "New Todos",
      })
      .then((res) => {
        setTodo(null);
      })
      .catch((error) => {
        message.error(error.message);
      });
  };

  const deleteTodo = (id) => {
    setOpenConfirm(false);
    axios
      .delete(`${hostUrl}/activity-groups/${id}`)
      .then((res) => {
        setTodo(null);
        setErrorOpen(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function ErrorModal() {
    return (
      <>
        <div data-cy="modal-information-icon">
          <Modal
            centered
            open={errorOpen}
            width={500}
            onCancel={() => {
              setErrorOpen(false);
            }}
            footer={[]}
          >
            <div
              data-cy="modal-information-text"
              style={{
                display: "flex",
                alignItems: "center",
                textAlign: "start",
                justifyContent: "space-evenly",
              }}
            >
              <p>
                <WarningOutlined style={{ color: "red", fontSize: 22 }} />{" "}
                Terjadi kesalahan. Gagal memuat list activity
              </p>
            </div>
          </Modal>
        </div>
      </>
    );
  }

  function InformationModal() {
    return (
      <>
        <div data-cy="modal-information">
          <Modal
            centered
            open={errorOpen}
            width={500}
            onCancel={() => {
              setErrorOpen(false);
            }}
            footer={[]}
          >
            <div
              data-cy="modal-information-text"
              style={{
                display: "flex",
                alignItems: "center",
                textAlign: "start",
                justifyContent: "space-evenly",
              }}
            >
              <p>
                <InfoCircleOutlined style={{ color: "red", fontSize: 22 }} />{" "}
                Berhasil menghapus Activity
              </p>
            </div>
          </Modal>
        </div>
      </>
    );
  }

  function DeleteModal() {
    return (
      <div data-cy="modal-delete">
        <Modal
          centered
          open={openConfirm}
          onCancel={() => {
            setOpenConfirm(false);
          }}
          width={1000}
          footer={[]}
        >
          <Result
            data-cy="confirm-dialog-text"
            status="warning"
            title={`Apakah anda yakin menghapus item ${currentData?.title}`}
            extra={
              <>
                <Button
                  data-cy="modal-delete-cancel-button"
                  onClick={() => {
                    setOpenConfirm(false);
                  }}
                  type="primary"
                  key="console"
                  style={{
                    background: "#F4F4F4",
                    borderRadius: 20,
                    width: 150,
                    color: "black",
                  }}
                  size="large"
                >
                  Batal
                </Button>
                <Button
                  data-cy="modal-delete-confirm-button"
                  onClick={() => {
                    deleteTodo(currentData?.id);
                  }}
                  style={{
                    background: "#ED4C5C",
                    borderRadius: 20,
                    width: 150,
                  }}
                  type="primary"
                  key="console"
                  size="large"
                >
                  Hapus
                </Button>
              </>
            }
          />
        </Modal>
      </div>
    );
  }

  return (
    <div style={{ minHeight: "100vh", padding: 20 }}>
      <ErrorModal />
      <InformationModal />
      <DeleteModal data-cy="modal-delete" />
      <div style={{ display: "block", margin: "0 2%" }}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            marginBottom: 50,
            padding: "0 20px",
          }}
        >
          <h1 data-cy="activity-title">Activity</h1>
          <Button
            data-cy="activity-add-button"
            style={{ borderRadius: 20 }}
            type="primary"
            icon={<PlusOutlined />}
            size="large"
            onClick={() => {
              addTodo();
            }}
          >
            Tambah
          </Button>
        </div>

        {todo?.data?.length > 0 ? (
          <List
            grid={{
              gutter: 16,
              column: 4,
            }}
            dataSource={todo?.data}
            renderItem={(item) => (
              <List.Item>
                <CardItems
                  id={item?.id}
                  title={item?.title}
                  created_at={item?.created_at}
                />
              </List.Item>
            )}
          />
        ) : (
          <>
            {todo?.total === 0 ? (
              <>
                <div
                  data-cy="todo-empty-state"
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    addTodo();
                  }}
                >
                  <img height={400} src={Image1} />
                </div>
              </>
            ) : (
              <>
                <Spin
                  indicator={
                    <LoadingOutlined
                      style={{
                        fontSize: 24,
                      }}
                      spin
                    />
                  }
                ></Spin>
              </>
            )}
          </>
        )}
      </div>
    </div>
  );
};
